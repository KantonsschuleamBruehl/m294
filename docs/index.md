# 0. Handlungsziele

Herzlich Willkommen auf den Doku-Seiten zum ÜK Modul 294 - Frontend einer interaktiven Webapplikation realisieren

![](./img/hello.png)

# Handlungsziele und handlungsnotwendige Kenntnisse

??? success "1. Richtet die lokale Entwicklungs- und Laufzeitumgebung so ein, dass ein vorgegebenes Projekt entwickelt werden kann."

    1. Kennt die für die Entwicklung zu installierenden Komponenten (z.B. welche Softwarebibliotheken benötigt werden).

    2. Kennt Vorgehensweisen, um an entsprechende Installationsanleitungen zu gelangen.

??? success "2. Programmiert mittels vorgegebener Technologie und mit Hilfe eines existierenden, dokumentierten Back-Ends ein effizientes, strukturiertes Front-End einer interaktiven Webapplikation, welches die Verwaltung (Create, Read, Update, Delete) von Daten ermöglicht und hält sich dabei an relevante Vorgaben." 

    1. Kennt grundlegende Prinzipien aktueller Umsetzungsarten für das Front-End einer interaktiven Webapplikation (z.B. Single Page Application mit Javascript oder Typescript).

    2. Kennt Möglichkeiten, wie Daten im Front-End einer interaktiven Webapplikation gehalten und mit einem Back-End synchronisiert werden können.

    3. Kennt mindestens eine Möglichkeit, mit einem entfernten Back-End zu kommunizieren und Daten nach CRUD auszutauschen.

??? success "3. Programmiert das Front-End einer interaktiven Webapplikation so, dass die einzelnen CRUD-Elemente des Front-Ends über einen permanenten Link erreichbar sind. "

    1.  Kennt Techniken, wie in einem Front-End einer interaktiven Webapplikation spezifische Teile einer Webapplikation (z.B. Detailsicht eines ausgewählten Datensatzes aus einer Liste) über einen permanenten Link erreichbar gemacht werden (z.B. client side routing).

??? success "4. Überprüft Zwischenergebnisse mit den Anforderungen (funktional, nichtfunktional, Sicherheit) und nimmt laufend Korrekturen vor."

    1.
    Kennt clientseitige Möglichkeiten zur Validierung von Eingabedaten.

    2.
    Kennt die wichtigsten Sicherheitsmassnahmen im Umgang mit Eingabedaten (z.B. input sanitization/validation, CORS, HTTPS).

    3.
    Kennt Techniken, um die Erfüllung von Anforderungen zu testen (z.B. automatisierte oder manuelle Tests, Logging, Debugging).

??? success "5. Hält vorgegebene Coderichtlinien ein und überprüft laufend deren Einhaltung. "

    1.
    Kennt Techniken/Methoden zur Konfiguration entsprechender Hilfsmittel, so dass diese fortlaufend auf Verletzung von Coderichtlinien hinweisen.

    2.
    Kennt mindestens eine Vorgehensweise zur Korrektur verletzter Coderichtlinien.

??? success "6. legt Änderungen und Erweiterungen der Implementierung übersichtlich und zuverlässig in einem Softwareverwaltungssystem ab." 

    1. Kennt die grundlegende Bedienung und den Workflow eines Softwareverwaltungssystems.

??? success "7. Schützt mindestens einen Bereich des Front-Ends vor anonymen Zugriffen."

    1.
    Kennt mindestens ein aktueller Authentifizierungsstandard (z.B. JWT) und dessen Implementierung im Front-End (z.B., um zwischen lesendem oder bearbeitendem Zugriff zu unterscheiden).
