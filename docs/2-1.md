# Angular - die Basics Teil 1

## Die app-root Komponente

Aktuell sehen wir in unserer ersten Angular App noch fast nichts. Wir können nun unter **app.component.html** Code hinzufügen. Man kann dort z.B. `<h3> Ich bin eine App Komponente </h3>` eingeben und es wird im Browser angezeigt. Aber wie weiss der Browser denn, dass er genau auf
diese Komponente zugreifen muss?

Wie Sie es bereits aus dem normalen Webdesign kennen, greift der Browser immer auf das index.html zu. Wenn wir uns diese Datei genauer ansehen, sehen wir im `<body>` den Zugriff auf
`<app-root>Loading...</app-root>`. App-root ist dabei kein gewöhnliches HTML Element, sondern unsere eigene Komponente, der wir einen individuellen Namen geben können.

Die Root-Komponente, welche alles zusammenhält und standardmässig von Angular erstellt wird, heisst app-root. Alle Komponenten, welche wir in Zukunft erstellen werden sind Subkomponenten der app-root und sind mit der app-root verbunden.

Wie vorher schon angesprochen, hat eine Komponente immer ein .html (Template), ein .css (Style) und ein .ts (Typescript) File. In der app.component.ts Datei sehen wir bei @Component den Selector Namen, also den Namen der Komponente. So können wir darauf zugreifen. Das @Component wird in Angular als Decorator bezeichnet, es betitelt die verschiedenen Teile des Codes und liefert Angular dadurch wertvolle Informationen.

![Bild4](img/bild4.png 'App-Komponente Decorator')

Diese Information müssen wir dem index.html geben, damit das index.html die Komponenten anzeigen kann. Wie genau Angular nun gestartet wird, sehen wir nicht direkt im Code.

## Erstellung einer eigenen Komponente

Die ganze Applikation, die ihr baut, ist eine Ansammlung von Komponenten. Eine Komponente ist z.B. die Navigationsbar oder die einzelnen Elemente einer Navigationsbar.
Alle Komponenten werden zu der App-root Komponente hinzugefügt (nicht direkt zum index.html).

Wir möchten nun eine Komponente bauen, welche uns Informationen über einen Server gibt. Wir erstellen für alle Komponenten einen eigenen Ordner, welcher gleich wie die Komponente heisst und sich im Ordner app befindet. Wir nennen den Ordner **server**.
Innerhalb des Ordners erstellen wir die einzelnen Files. Das erste File heisst `server.component.ts` Eine Komponente ist nur eine Typescript Klasse. Wir müssen also die Klasse exportieren, damit Angular darauf basierend Objekte erstellen kann.

Wir benutzen einen @Component Decorator, um Angular zu sagen wie die Komponente heisst. Component muss zuvor noch vom Angular Core importiert werden.
Der Selector muss einzigartig sein. Wir nennen ihn 'app-server'. Unter der Eigenschaft 'templateUrl' muss man den Pfad zum HTML angeben. Die HTML Datei besteht aktuell noch nicht, deshalb müssen wir sie erstellen. Das HTML-File hat den Namen server.component.html.
In dem server.component.html File kann ich nun HTML Code eingeben. Wir erfassen einen Paragraphen mit der Bezeichnung `<p>Die Server Komponente</p>`.
Im Typescript File müssen wir dann noch direkt zu dem HTML zeigen, indem wir templateUrl noch hinzufügen.

![Bild5](img/bild5.png 'Die Server-Komponente')

## Die Rolle von der App Komponente und Deklaration von Komponenten

Damit wir die Server-Komponente nun nutzen können, müssen wir die Server Komponente in der App Komponente importieren.
Das App Komponente bündelt die verschiedenen Komponenten in Pakete zusammen. Man muss die Komponente in der App Komponente registrieren. Das machen wir im Imports Array.
Zusätzlich müssen wir noch ein import Statement machen, wo wir Typescript sagen, wo es die
Server Komponenten finden kann (s. Screenshot).

![Bild6](img/bild6.png 'Registrierung der Server-Komponente')

Nun ist die Komponente bei der App Komponente registriert und damit nutzbar. Wechseln Sie nun in die server.component.html und schreiben Sie dort folgenden Code hinein: `<h1> I am the Server Component <h1>` Nun können wir unsere Komponente verwenden. Wir gehen zur app.component.html und füge dort
das `<app-server></app-server>` Element hinzu. Nun sollte die Server-Komponente auf der Website
angezeigt werden. Achtung: Achten Sie darauf, dass Sie immer den richtigen Namen des Selectors für die Komponente verwenden.

![Bild7](img/bild7.png 'Darstellung der Server-Komponente im Browser')

## Erstellung von Komponenten über die CLI und das Nesting von Komponenten

Mit `ng generate component servers` kann man eine neue Komponente automatisch erstellen. Es wird automatisch ein neuer Ordner im App Ordner erstellt. Ausserdem wird schon ein ts, html und css File erstellt.

Jetzt können wir im **servers.component.html** z.B. die vorhin manuell erstellte `<app-server></app-server>` so oft einfügen wie wir wollen. Das ist der Vorteil von Komponenten. Man kann
sie beliebig oft verwenden.

![Bild8](img/bild8.png 'Erstellung einer automatischen Komponente')

Nun müssen wir in der **app.component.html** noch `<app-server>` mit `<app-servers>` wechseln, damit wir auf die neue Komponente zugreifen. Wenn wir nun zum Browser zurückgehen sollten wir zwei Server Components sehen.

## Component Templates und Styles

Jede Komponente benötigt ein Template (das HTML File). Wir können das HTML natürlich auch stylen. Wir wechseln zum app.component.css, wo wir h3 verändern und es blau machen. Wie dies funktioniert sollten Sie bereits in früheren Modulen gelernt haben, deshalb fehlt hier der Screenshot.
So kann man das .css File verwenden. Alternativ kann man natürlich auch herkömmliche
Bootstrap Klassen verwenden (wie Sie es auch vom "normalen Webdesign" kennen).

![Bild9](img/bild9.png 'Erstellung einer automatischen Komponente')

## Fazit

Bis jetzt sollten Sie verstanden haben wie man Komponenten in Angular erstellt und wie diese ineinandergreifen, damit man sie schlussendlich auf der Website erkennen kann. Im nächsten Kapitel werden wir uns ansehen wie Daten von einer Komponenten zur anderen Komponenten hin und hergesendet werden können.
