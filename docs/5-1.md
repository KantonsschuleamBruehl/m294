# Authentifizierung

## Wie funktioniert eine Authentifizierung?

Sich zu authentifizieren bedeutet, dass man sich bei einer App einloggen kann. Bisher haben wir keine Authentifizierung bei unseren Apps benutzt. In diesem Kapitel lernen wir, wie man bestimmte Routen nur mit erfolgreicher Authentifizierung schützen kann. Zunächst muss man mal verstehen wie eine Authentifizierung im Hintergrund funktioniert.

Wir haben einen Client, welcher den Browser benutzt und einen Server. Wenn der Client seine Login-Daten eingibt, dann werden diese Daten an den Server gesendet und dieser überprüft ob die Login-Daten richtig sind.
Wenn die Daten stimmen, sendet der Server dem Client einen Token (JSON Web Token). Das ist ein String, welcher entpackt werden kann vom Kunden. Der Token ist auf dem Server generiert. Nur dieser Server kennt den Token. Die Idee dabei ist, dass der Client diesen Token bei jedem Request mitsendet, welcher eine Authentifizierung benötigt. Wenn wir uns z.B. authentifizieren müssen, um neue To Dos anzulegen, dann muss dieser Token jedes Mal mitgesendet werden. Hier ist dies nochmals visualisiert:

![Bild80](img/bild80.png 'Authentifizierung')

## Erstellung einer Authentication Page

Zuerst müssen wir eine Seite erstellen, auf welcher man sich einloggen kann. Wir machen dafür mit unserer Recipe Website weiter. Wir erstellen einen neuen Ordner im app Ordner namens **auth**. In der **auth** Komponente erstellen wir das Template **auth.component.hmtl** und das Typescript File **auth.component.ts**. Im **auth.component.ts** File benötigen wir folgenden Standardcode:

![Bild81](img/bild81.png 'Authentifizierung')

Die Komponente muss ebenfalls noch registriert werden. Dazu wechseln wir zum **app.module.ts** File und fügen es bei Declarations hinzu. Ebenfalls ist sicherzustellen, dass die Auth Komponente importiert wird. Der Code sieht wie folgt aus:

![Bild82](img/bild82.png 'Authentifizierung')

Nun ist unsere Komponente registriert und wir können damit arbeiten. Wir starten mit dem Template und fügen dort die Form für das Login hinzu.
Der Code dafür sieht wie folgt aus:

![Bild83](img/bild83.png 'Authentifizierung')

Im **app-routing-module.ts** Modul möchte ich nun einen neuen Pfad resgistrieren. Wir nehmen z.B. diesen Pfad:

![Bild84](img/bild84.png 'Authentifizierung')

Auch hier müssen wir wieder sicherstellen, dass die Auth Komponente in **app.routing-module.ts** importiert worden ist (ansonsten ist kein Zugriff möglich).

Wenn man dies nun speichert und auf localhost:4200/auth geht, sieht man die gerade erstellte Seite.

Nun werden wir im heder Template **header.component.html** noch einen Button zur Auth Seite hinzufügen.

![Bild85](img/bild85.png 'Authentifizierung')

## Wechseln zwischen Login und Registrierung

