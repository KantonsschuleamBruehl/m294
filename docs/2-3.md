# Angular - die Basics Teil 2

## Data Directives

Directives sind Instruktionen, welche wir dem Document Object Model (DOM) geben. Nochmals zur Erinnerung: Das DOM bezeichnet die Baumartige Struktur, welche der Browser erstellt, um die HTML Elemente in der richtigen Reihenfolge auf der Website zu platzieren.
Man kann Directives selbst schreiben. Angular hat bereits einige Directives eingebaut, welche wir uns im Folgenden ansehen werden:

## NgIf

Wenn wir z.B. **Server with ID 10 is offline** nur anzeigen möchten, wenn der Server wirklich offline ist, können wir eine Bedingung einbauen. Es funktioniert ähnlich dem If-statement.
Wir erstellen zuerst einen Paragraph in der **server.component.html**. Wir können nun
eine Directive hinzufügen. Wir benötigen `ngIf`.
Wir können damit Elemente beim DOM hinzufügen oder entfernen. Natürlich muss noch eine entsprechende Property mit dem Namen `serverCreated` im Typescript File hinzugefügt werden, damit die ngIf Anweisung auch ausgewertet werden kann. Eingesetzt sieht der Code im Template wie folgt aus:

![Bild17](img/bild17.png 'ng condition')

## NgStyle

Um ein HTML Element dynamisch zu stylen, können wir auf ngstyle zurückgreifen. Wir öffnen dazu **server.component.ts** (Achtung: nicht serverS) und fügen einen constructor hinzu. Diese Methode wird aufgerufen, sobald ein Objekt der Klasse erstellt wird. Wir können dem Server Status jetzt z.B. einen zufälligen Wert zwischen 0 und 1 vergeben. Wenn der Wert grösser als 0.5 ist, will ich es zu
online setzen, ansonsten zu offline. Je nachdem welcher Status angezeigt wird, wollen wir die Farbe verändern. Dafür können wir ngstyle im **server.component.html** verwenden. Mit Property Binding können wir ein Property zum ngStyle binden, nämlich das Property, welches wir verändern möchten. Die ngStyle Komponente verlangt in den Klammern die Veränderung. Hier der Code:

![Bild18](img/bild18.png 'ng condition')

Da es sich hier um Typescript Code handelt können wir die Methode `getColor()` aufrufen. Diese Methode existiert noch nicht und muss zuerst in der **server.comoponent.ts** aufgerufen werden. Hier der Code für das **server.component.ts** file:

![Bild19](img/bild19.png 'ng condition')

Wenn wir das nun ausführen, sehen wir beim laufenden Server einen grünen Hintergrund, beim nicht laufenden Server einen grünen Hintergrund.

## NgClass

NgStyle hilft uns dynamisch andere Styles anzuwenden. NgClass macht dasselbe für Klassen. Wir können also dynamisch Klassen hinzufügen. Um dies mal auszuprobieren, fügen wir im File **server.component.ts** die Klasse online hinzu. In dieser Klasse fügen wir die Textfarbe weiss hinzu:

![Bild20](img/bild20.png 'NgClass')

wir fügen dieses ngClass nun auch in der **server.component.html** hinzu. Wir verwenden dort ebenfalls wieder eckige Klammern und das Property binding. Wir geben dem Attribut-Directive ein JavaScript Objekt. Ich möchte die Klasse nur hinzufügen, wenn der serverStatus auf online gesetzt ist. Wenn der serverStatus nicht auf online ist, soll die Klasse nicht hinzugefügt werden.

![Bild21](img/bild21.png 'Ng Class HMTL')

Nun sollten Sie im Browser sehen können, dass die Textfarbe bei einem aufgeschalteten Server auf weiss wechselt.

## NgFor

In unserer App-Komponente haben wir bis jetzt manuell die Server hinzugefügt. Es wäre spannender die Server dynamisch hinzuzufügen. Dazu erstellen wir zuerst einen array mit vorhanden Servern. Ich möchte nun bei Erstellung eines neuen Servers den Array vergrössern.Dazu wechseln wir in das Typescript File **server.component.ts** (nicht server.component.ts) und erstellen den Array. Hier der Code.

![Bild22](img/bild22.png 'Ng Class TS')

Ich möchte nun im HTML File die aktuelle Liste anzeigen. Dazu füge ich die Struktur-Direktive mit einem Stern hinzu. `ngFor` loopt durch alle Elemente der Liste durch.

![Bild23](img/bild23.png 'Ng Class HMTL')

Testen Sie, ob immer noch alle Server hinzugefügt werden. Sie können auch einen weiteren Server im Array hinzufügen.
